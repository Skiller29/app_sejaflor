﻿using PCLExt.FileStorage.Folders;
using Plugin.FilePicker.Abstractions;
using SejaFlorApp.Droid.Database;
using SejaFlorApp.Interfaces;
using System;
using System.IO;
using System.Threading.Tasks;
using Xamarin.Forms;

[assembly: Dependency(typeof(Repository_Android))]
namespace SejaFlorApp.Droid.Database
{
    public class Repository_Android : IRepository
    {             
        public async Task<bool> MakeBackupAndSaveInDownloadsPathAsync()
        {
            try
            {
                var folderRoot = new LocalRootFolder();
                var database = await folderRoot.GetFileAsync("banco.db3");
                var downloadPath = Android.OS.Environment.GetExternalStoragePublicDirectory(Android.OS.Environment.DirectoryDownloads).Path;
                if (database != null)
                    File.Copy(database.Path, Path.Combine(downloadPath, database.Name), true);

                return true;
            }
            catch (Exception ex)
            {
                return false;
            }           
        }

        public async Task<bool> ImportDatabase(FileData database)
        {
            try
            {
                var folderRoot = new LocalRootFolder();
                var path = Path.Combine(folderRoot.Path, database.FileName);
                File.WriteAllBytes(path, database.DataArray);                
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }            
        }
    }
}