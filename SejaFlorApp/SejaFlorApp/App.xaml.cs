﻿using System;
using Xamarin.Forms;
using SejaFlorApp.Views;
using Xamarin.Forms.Xaml;
using SejaFlorApp.Views.Clients;
using SQLite;
using PCLExt.FileStorage.Folders;
using PCLExt.FileStorage;

[assembly: XamlCompilation (XamlCompilationOptions.Compile)]
namespace SejaFlorApp
{
	public partial class App : Application
	{
        public SQLiteConnection Conexao { get; private set; }

        public App ()
		{
            //Register Syncfusion license
            Syncfusion.Licensing.SyncfusionLicenseProvider.RegisterLicense("MTA1NjhAMzEzNjJlMzIyZTMwVHVUUEJ3VFBFblBEb3MwemZ6QkRRNHJTb0FEZjFSdjc0VG4wSzJVZDJQRT0=");

            InitializeComponent();            
            MainPage = new IndexPage() { Detail = new NavigationPage(new MainPage()), Master = new MasterPage()};            
        }

		protected override void OnStart ()
		{
			// Handle when your app starts
		}

		protected override void OnSleep ()
		{
			// Handle when your app sleeps
		}

		protected override void OnResume ()
		{
			// Handle when your app resumes
		}
	}
}
