﻿using System;
using System.Collections;
using System.Linq;
using Xamarin.Forms;
namespace SejaFlorApp.Converters
{
     public class CollectionEmptyToBool : IValueConverter
    {
		public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            var ie = value as IEnumerable;
                                      
            var reverse = parameter != null;
            var val = ie == null || IsEmpty(ie);

            return reverse ? val : !val;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }

        private bool IsEmpty(IEnumerable en)
        {
            return !en.Cast<object>().Any();
        }
    }
}
