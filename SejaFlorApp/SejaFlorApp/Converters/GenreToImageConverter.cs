﻿using SejaFlorApp.Models.Enum;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;
using Xamarin.Forms;

namespace SejaFlorApp.Converters
{
    public class GenreToImageConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var genre = (Genre)value;
            if (genre == Genre.Female)
                return "female.png";
            else if(genre == Genre.Male)
                return "male.png";

            return "";
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var genre = value.ToString();
            if (genre.Equals("female.png"))
                return Genre.Female;
            else
                return Genre.Male;            
        }
    }
}
