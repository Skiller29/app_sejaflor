﻿using SejaFlorApp.Models.Enum;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;
using Xamarin.Forms;

namespace SejaFlorApp.Converters
{
    public class TypePaymentInCashToBoolConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var payment = (TypePayment)value;
            if(parameter == null)
            {
                if (payment == TypePayment.Cash)
                    return true;
                else
                    return false;
            }

            if (payment == TypePayment.Cash)
                return false;
            else
                return true;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
