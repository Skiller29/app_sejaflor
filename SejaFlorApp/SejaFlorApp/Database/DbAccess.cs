﻿
using PCLExt.FileStorage;
using PCLExt.FileStorage.Folders;
using SejaFlorApp.Interfaces;
using SejaFlorApp.Models;
using SQLite;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace SejaFlorApp.Database
{
    public class DbAccess<T> : IDisposable where T : class, new()
    {
        private SQLiteAsyncConnection _sqLiteConnection;

        public DbAccess()
        {
            var pasta = new LocalRootFolder();
            var arquivo = pasta.CreateFile("banco.db3", CreationCollisionOption.OpenIfExists);            

            _sqLiteConnection = new SQLiteAsyncConnection(arquivo.Path);            
            _sqLiteConnection.CreateTableAsync<Client>();            
            _sqLiteConnection.CreateTableAsync<Product>();
            _sqLiteConnection.CreateTableAsync<Sale>();
            _sqLiteConnection.CreateTableAsync<ItemSale>();
            _sqLiteConnection.CreateTableAsync<Installment>();
        }

        public async Task SaveAsync(T item)
        {
            await _sqLiteConnection.InsertAsync(item);
        }
     
        public async Task<IEnumerable<T>> ListAllAsync()
        {            
            return await _sqLiteConnection.Table<T>().ToListAsync();
        }       

        public async Task<IEnumerable<T>> ListAsync(Expression<Func<T, bool>> predicate)
        {
            return await _sqLiteConnection.Table<T>().Where(predicate).ToListAsync();
        }

        public async Task<T> FindAsync(Expression<Func<T, bool>> predicate)
        {
            return await _sqLiteConnection.Table<T>().FirstOrDefaultAsync(predicate);
        }
        
        public async Task DeleteAsync(int id)
        {
            await _sqLiteConnection.DeleteAsync<T>(id);
        }

        public async Task UpdateAsync(T item)
        {
            await _sqLiteConnection.UpdateAsync(item);
        }

        public void Dispose()
        {
            
        }
    }
}
