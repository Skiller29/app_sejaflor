﻿using Plugin.FilePicker.Abstractions;
using System.Threading.Tasks;

namespace SejaFlorApp.Interfaces
{
    public interface IRepository
    {
        Task<bool> MakeBackupAndSaveInDownloadsPathAsync();
        Task<bool> ImportDatabase(FileData database);
    }
}
