﻿using SejaFlorApp.Models.Enum;
using SQLite;
using System;
using System.Collections.Generic;

namespace SejaFlorApp.Models
{
    public class Client
    {
        [PrimaryKey, AutoIncrement]
        public int Id { get; set; }

        public string Name { get; set; }
        public string Phone { get; set; }
        public string Address { get; set; }
        public string Neighborhood { get; set; }
        public Genre Genre { get; set; }
        public string Reference { get; set; }
        public DateTime CreationDate { get; set; }

        [Ignore]
        public List<Sale> Sales { get; set; }
    }
}
