﻿
namespace SejaFlorApp.Models.Enum
{
    public enum TypePayment
    {
        Cash,
        CreditCard,
        DebitCard,
        Financing
    }
}
