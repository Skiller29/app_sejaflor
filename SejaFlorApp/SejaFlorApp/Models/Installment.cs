﻿using SQLite;
using System;

namespace SejaFlorApp.Models
{
    public class Installment
    {
        [PrimaryKey, AutoIncrement]
        public int Id { get; set; }

        public DateTime DueDate { get; set; }
        public decimal Value { get; set; }
        public int NumberInstallment { get; set; }
        public bool IsPaid { get; set; }
        public DateTime PaymentDate { get; set; }

        public int SaleID { get; set; }

        [Ignore]
        public Sale Sale { get; set; }

        [Ignore]
        public string ValueFormatted
        {
            get
            {
                return string.Format("R$ {0:N}", Value);
            }
        }
    }
}
