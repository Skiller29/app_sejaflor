﻿using SQLite;

namespace SejaFlorApp.Models
{
    public class ItemSale
    {
        [PrimaryKey, AutoIncrement]
        public int Id { get; set; }
        
        public int Quantity { get; set; }
        public decimal Amount { get; set; }

        public int ProductID { get; set; }

        public int SaleID { get; set; }

        [Ignore]
        public Product Product { get; set; }

        [Ignore]
        public Sale Sale { get; set; }

        [Ignore]
        public string TotalFormatted
        {
            get
            {
                if (Product == null || Sale == null)
                    return "";


                return string.Format("R$ {0:N}", (Sale.TypePayment == Enum.TypePayment.Cash ? Product.SaleAmountInCash : Product.SaleAmountInCard) * Quantity);
            }
        }
    }
}
