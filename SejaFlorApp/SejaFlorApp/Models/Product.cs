﻿using SQLite;
using System;
using System.Collections.Generic;

namespace SejaFlorApp.Models
{
    public class Product
    {
        [PrimaryKey, AutoIncrement]
        public int Id { get; set; }

        public string Name { get; set; }        
        public decimal CostAmount { get; set; }
        public decimal SaleAmountInCash { get; set; }
        public decimal SaleAmountInCard { get; set; }
        public DateTime CreationDate { get; set; }

        [Ignore]
        public List<ItemSale> ItemsSale { get; set; }

        public string SaleAmountInCashFormatted
        {
            get
            {
                return string.Format("R$ {0:N}", SaleAmountInCash);
            }
        }      
    }
}
