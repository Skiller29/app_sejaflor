﻿using SejaFlorApp.Models.Enum;
using SQLite;
using System;
using System.Collections.Generic;

namespace SejaFlorApp.Models
{
    public class Sale
    {
        [PrimaryKey, AutoIncrement]
        public int Id { get; set; }

        public DateTime SaleDate { get; set; }        
        public decimal Total { get; set; }
        public decimal TotalCost { get; set; }
        public TypePayment TypePayment { get; set; }
        public bool Installment { get; set; }
        public int NumberInstallments { get; set; }
        public DateTime DueDate { get; set; }

        public int ClientID { get; set; }

        [Ignore]
        public Client Client { get; set; }

        [Ignore]
        public List<ItemSale> ItemsSale { get; set; }

        [Ignore]
        public List<Installment> Installments { get; set; }

        [Ignore]
        public string TotalFormatted
        {
            get
            {
                return string.Format("R$ {0:N}", Total);
            }
        }

        [Ignore]
        public decimal Profit
        {
            get { return Total - TotalCost; }
        }
        
    }
}
