﻿using SejaFlorApp.Database;
using SejaFlorApp.Helpers;
using SejaFlorApp.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace SejaFlorApp.Services
{
    public class ClientService
    {
        private readonly DbAccess<Client> _access;
        private readonly DbAccess<Sale> _accessSale;

        public ClientService()             
        {
            _access = new DbAccess<Client>();
            _accessSale = new DbAccess<Sale>();
        }

        public async Task InsertAsync(Client client)
        {
            DomainException.When(client == null, "Por vavor, preencha todos os campos");
            DomainException.When(string.IsNullOrEmpty(client.Name), "Por favor, digite o nome do cliente.");
            client.CreationDate = DateTime.Now;

            await _access.SaveAsync(client);        
        }

        public async Task EditAsync(Client client)
        {
            DomainException.When(client == null, "Por vavor, preencha todos os campos");
            DomainException.When(string.IsNullOrEmpty(client.Name), "Por favor, digite o nome do cliente.");
            await _access.UpdateAsync(client);
        }

        public async Task<Client> LoadAsync(int id)
        {
            var client = await _access.FindAsync(x => x.Id == id);
            if (client != null)
                client.Sales = (await _accessSale.ListAsync(s => s.ClientID == client.Id)).ToList();

            return client;
        }

        public async Task RemoveAsync(int id)
        {
            await _access.DeleteAsync(id);
        }

        public async Task<List<Client>> ListAsync(Expression<Func<Client, bool>> predicate)
        {
            var clients = (await _access.ListAsync(predicate)).ToList();
            foreach (var client in clients)
                client.Sales = (await _accessSale.ListAsync(s => s.ClientID == client.Id)).ToList();

            return clients;
        }

        public async Task<List<Client>> ListAllAsync()
        {
            var clients = (await _access.ListAllAsync()).ToList();
            foreach (var client in clients)
                client.Sales = (await _accessSale.ListAsync(s => s.ClientID == client.Id)).ToList();

            return clients;
        }
    }
}
