﻿using SejaFlorApp.Database;
using SejaFlorApp.Helpers;
using SejaFlorApp.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace SejaFlorApp.Services
{
    public class ProductService
    {
        private readonly DbAccess<Product> _access;
        private readonly DbAccess<ItemSale> _accessItemSale;

        public ProductService()
        {
            _access = new DbAccess<Product>();
            _accessItemSale = new DbAccess<ItemSale>();
        }

        public async Task InsertAsync(Product product)
        {
            DomainException.When(product == null, "Por vavor, preencha todos os campos");
            DomainException.When(string.IsNullOrEmpty(product.Name), "Por favor, digite o nome do produto.");
            DomainException.When(product.CostAmount == 0, "Por favor, digite o valor de custo do produto.");
            DomainException.When(product.SaleAmountInCash == 0, "Por favor, digite o valor de venda em dinheiro do produto.");
            DomainException.When(product.SaleAmountInCard == 0, "Por favor, digite o valor de venda no cartão de crédito do produto.");
            product.CreationDate = DateTime.Now;

            await _access.SaveAsync(product);
        }

        public async Task EditAsync(Product product)
        {
            DomainException.When(product == null, "Por vavor, preencha todos os campos");
            DomainException.When(string.IsNullOrEmpty(product.Name), "Por favor, digite o nome do produto.");
            DomainException.When(product.CostAmount == 0, "Por favor, digite o valor de custo do produto.");
            DomainException.When(product.SaleAmountInCash == 0, "Por favor, digite o valor de venda em dinheiro do produto.");
            DomainException.When(product.SaleAmountInCard == 0, "Por favor, digite o valor de venda no cartão de crédito do produto.");
            await _access.UpdateAsync(product);
        }

        public async Task<Product> LoadAsync(int id)
        {
            var product = await _access.FindAsync(x => x.Id == id);
            if (product != null)
                product.ItemsSale = (await _accessItemSale.ListAsync(i => i.SaleID == product.Id)).ToList();

            return product;
        }

        public async Task RemoveAsync(int id)
        {
            await _access.DeleteAsync(id);
        }

        public async Task<List<Product>> ListAsync(Expression<Func<Product, bool>> predicate)
        {
            var products = (await _access.ListAsync(predicate)).ToList();
            foreach (var product in products)
                product.ItemsSale = (await _accessItemSale.ListAsync(i => i.SaleID == product.Id)).ToList();

            return products;
        }

        public async Task<List<Product>> ListAllAsync()
        {
            var products = (await _access.ListAllAsync()).ToList();
            foreach (var product in products)
                product.ItemsSale = (await _accessItemSale.ListAsync(i => i.SaleID == product.Id)).ToList();

            return products;
        }
    }
}
