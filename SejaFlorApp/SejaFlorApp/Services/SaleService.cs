﻿using SejaFlorApp.Database;
using SejaFlorApp.Helpers;
using SejaFlorApp.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace SejaFlorApp.Services
{
    public class SaleService
    {
        private readonly DbAccess<Sale> _access;
        private readonly DbAccess<ItemSale> _accessItemSale;
        private readonly DbAccess<Installment> _accessInstallment;
        private readonly DbAccess<Client> _accessClient;
        private readonly DbAccess<Product> _accessProduct;

        public SaleService()
        {
            _access = new DbAccess<Sale>();
            _accessItemSale = new DbAccess<ItemSale>();
            _accessInstallment = new DbAccess<Installment>();
            _accessClient = new DbAccess<Client>();
            _accessProduct = new DbAccess<Product>();
        }

        public async Task InsertAsync(Sale sale, List<ItemSale> products)
        {
            DomainException.When(sale == null, "Por vavor, preencha todos os campos");
            DomainException.When(sale.Client == null, "Por favor, escolha um cliente.");
            DomainException.When(products == null || !products.Any(), "Por favor, escolha no mínimo um produto.");

            await _access.SaveAsync(sale);

            
            foreach (var item in products)
            {
                var itemSale = new ItemSale()
                {
                    SaleID = sale.Id,
                    ProductID = item.Product.Id,
                    Quantity = item.Quantity,
                    Amount = sale.TypePayment == Models.Enum.TypePayment.Cash ? item.Product.SaleAmountInCash : item.Product.SaleAmountInCard
                };

                await _accessItemSale.SaveAsync(itemSale);
            }

            for (int i = 0; i < sale.NumberInstallments; i++)
            {
                var installment = new Installment()
                {
                    DueDate = sale.DueDate.AddMonths(i),
                    IsPaid = false,
                    NumberInstallment = i + 1,
                    SaleID = sale.Id,
                    Value = sale.Total / sale.NumberInstallments
                };
                await _accessInstallment.SaveAsync(installment);
            }
        }

        public async Task<Sale> LoadAsync(int id)
        {
            var sale = await _access.FindAsync(x => x.Id == id);
            if(sale != null)
            {
                sale.Installments = (await _accessInstallment.ListAsync(i => i.SaleID == sale.Id)).ToList();
                sale.ItemsSale = (await _accessItemSale.ListAsync(i => i.SaleID == sale.Id)).ToList();
                sale.Client = await _accessClient.FindAsync(c => c.Id == sale.ClientID);
            }
            return sale;
        }

        public async Task RemoveAsync(int id)
        {
            var itemsSale = await _accessItemSale.ListAsync(x => x.SaleID == id);
            if(itemsSale != null)
            {
                foreach (var item in itemsSale)
                {
                    await _accessItemSale.DeleteAsync(item.Id);
                }
            }
            
            
            var installments = await _accessInstallment.ListAsync(x => x.SaleID == id);
            if(installments != null)
            {
                foreach (var item in installments)
                {
                    await _accessInstallment.DeleteAsync(item.Id);
                }
            }            

            await _access.DeleteAsync(id);
        }

        public async Task<List<Sale>> ListAsync(Expression<Func<Sale, bool>> predicate)
        {
            var sales = (await _access.ListAsync(predicate)).ToList();
            foreach (var sale in sales)
            {
                sale.Installments = (await _accessInstallment.ListAsync(i => i.SaleID == sale.Id)).ToList();
                sale.ItemsSale = (await _accessItemSale.ListAsync(i => i.SaleID == sale.Id)).ToList();
                sale.Client = await _accessClient.FindAsync(c => c.Id == sale.ClientID);
            }
            return sales;
        }

        public async Task<List<Sale>> ListAllAsync()
        {
            var sales = (await _access.ListAllAsync()).ToList();
            foreach (var sale in sales)
            {
                sale.Installments = (await _accessInstallment.ListAsync(i => i.SaleID == sale.Id)).ToList();
                sale.ItemsSale = (await _accessItemSale.ListAsync(i => i.SaleID == sale.Id)).ToList();
                sale.Client = await _accessClient.FindAsync(c => c.Id == sale.ClientID);
            }

            return sales;
        }

        public async Task<List<ItemSale>> GetItemsSaleAsync(int idSale)
        {
            var itemsSale = (await _accessItemSale.ListAsync(x => x.SaleID == idSale)).ToList();
            var sale = await LoadAsync(idSale);            
            foreach (var item in itemsSale)
            {
                item.Sale = sale;
                item.Product = await _accessProduct.FindAsync(p => p.Id == item.ProductID);
            }
                
            return itemsSale;
        }

        public async Task<List<Sale>> FilterSalesAsync(DateTime initialDate, DateTime endDate)
        {
            var dateInitial = new DateTime(initialDate.Year, initialDate.Month, initialDate.Day, 0, 0, 0);
            var dateEnd = new DateTime(endDate.Year, endDate.Month, endDate.Day, 23, 59, 59);
            var sales = (await _access.ListAsync(x => x.SaleDate >= dateInitial && x.SaleDate <= dateEnd)).ToList();
            foreach (var sale in sales)
            {
                sale.Installments = (await _accessInstallment.ListAsync(i => i.SaleID == sale.Id)).ToList();
                sale.ItemsSale = (await _accessItemSale.ListAsync(i => i.SaleID == sale.Id)).ToList();
                sale.Client = await _accessClient.FindAsync(c => c.Id == sale.ClientID);
            }

            return sales;
        }

        #region Installments
        public async Task<List<Installment>> GetInstallmentsSaleAsync(int idSale)
        {
            var installments = (await _accessInstallment.ListAsync(x => x.SaleID == idSale)).ToList();
            var sale = await LoadAsync(idSale);
            foreach (var installment in installments)
                installment.Sale = sale;

            return installments;
        }

        public async Task PayInstallmentAsync(Installment installment)
        {
            await _accessInstallment.UpdateAsync(installment);
        }

        public async Task<List<Installment>> FilterInstallmentsAsync(DateTime initialDate, DateTime endDate)
        {
            var dateInitial = new DateTime(initialDate.Year, initialDate.Month, initialDate.Day, 0, 0, 0);
            var dateEnd = new DateTime(endDate.Year, endDate.Month, endDate.Day, 23, 59, 59);
            var installments = (await _accessInstallment.ListAsync(x => (x.DueDate >= dateInitial && x.DueDate <= dateEnd) || (x.PaymentDate >= dateInitial && x.PaymentDate <= dateEnd))).Distinct().ToList();
            foreach (var installment in installments)
                installment.Sale = await LoadAsync(installment.SaleID);

            return installments;
        }

        public async Task<bool> IsLastInstallmentAsync(int idInstallment, int idSale)
        {
            var lastInstallment = (await _accessInstallment.ListAsync(x => x.SaleID == idSale)).LastOrDefault();
            if (lastInstallment == null) return false;

            return lastInstallment.Id == idInstallment;
        }

        public async Task<bool> CanPayInstallmentAsync(int idInstallment)
        {
            var installment = await _accessInstallment.FindAsync(x => x.Id == idInstallment);
            if(installment != null)
            {
                var sale = await LoadAsync(installment.SaleID);
                if(sale != null)
                {
                    var result = await _accessInstallment.FindAsync(x => x.SaleID == sale.Id && x.DueDate < installment.DueDate && !x.IsPaid);
                    return result == null;
                }
                return false;
            }

            return false;
        }

        public async Task InsertNewInstallmentAsync(Installment installment)
        {
            await _accessInstallment.SaveAsync(installment);
        }

        public async Task UpdateInstallmentAsync(decimal value, int idInstallment, int idSale, bool hasLeft)
        {
            var installments = (await _accessInstallment.ListAsync(x => x.SaleID == idSale)).ToList();
            var installment = await _accessInstallment.FindAsync(x => x.Id == idInstallment);
            if(installments != null && installment != null)
            {
                var index = installments.FindIndex(x => x.Id == installment.Id);
                var nextInstallment = installments[index + 1];
                if (hasLeft)
                    nextInstallment.Value -= value;
                else
                    nextInstallment.Value += value;

                await _accessInstallment.UpdateAsync(nextInstallment);
            }
        }

        #endregion
    }
}
