﻿using Acr.UserDialogs;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using Xamarin.Forms;

namespace SejaFlorApp.ViewModels
{
    public class BaseViewModel : INotifyPropertyChanged
    {
        public Page Nav
        {
            get
            {
                var detail = App.Current.MainPage as MasterDetailPage;
                return detail.Detail;
            }
        }


        bool isBusy = false;
        public bool IsBusy
        {
            get { return isBusy; }
            set {
                SetProperty(ref isBusy, value);
                Loading();
            }
        }

        string title = string.Empty;
        public string Title
        {
            get { return title; }
            set { SetProperty(ref title, value); }
        }

        protected bool SetProperty<T>(ref T backingStore, T value,
            [CallerMemberName]string propertyName = "",
            Action onChanged = null)
        {
            if (EqualityComparer<T>.Default.Equals(backingStore, value))
                return false;

            backingStore = value;
            onChanged?.Invoke();
            OnPropertyChanged(propertyName);
            return true;
        }

        #region INotifyPropertyChanged
        public event PropertyChangedEventHandler PropertyChanged;
        protected void OnPropertyChanged([CallerMemberName] string propertyName = "")
        {
            var changed = PropertyChanged;
            if (changed == null)
                return;

            changed.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
        #endregion

        public void ShowMessage(string message)
        {
            UserDialogs.Instance.Toast(message, new TimeSpan(0, 0, 3));
        }

        public void Loading()
        {
            if (IsBusy)
                UserDialogs.Instance.ShowLoading("Carregando", MaskType.Clear);
            else
                UserDialogs.Instance.HideLoading();
        }
    }
}
