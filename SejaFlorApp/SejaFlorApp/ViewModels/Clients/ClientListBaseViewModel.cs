﻿using SejaFlorApp.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;

namespace SejaFlorApp.ViewModels.Clients
{
    public class ClientListBaseViewModel : BaseViewModel
    {
        private string _search;
        public string Search
        {
            get
            {
                return _search;
            }

            set
            {
                _search = value;
                OnPropertyChanged("Search");
                FilterClients(_search);
            }
        }

        private ObservableCollection<Client> _clients;
        public ObservableCollection<Client> Clients
        {
            get
            {
                return _clients;
            }

            set
            {
                _clients = value;
                OnPropertyChanged("Clients");
            }
        }

        private ObservableCollection<Client> _originClients;
        public ObservableCollection<Client> OriginClients
        {
            get
            {
                return _originClients;
            }

            set
            {
                _originClients = value;
                OnPropertyChanged("OriginClients");
            }
        }
        
        private void FilterClients(string op)
        {
            if (!string.IsNullOrEmpty(op))
            {
                Clients = new ObservableCollection<Client>(OriginClients.Where(x => x.Name.ToLower().Contains(op.ToLower())).OrderBy(x => x.Name));
            }
            else
            {
                Clients = OriginClients;
            }
        }
    }
}
