﻿using SejaFlorApp.Models;
using SejaFlorApp.Services;
using SejaFlorApp.Views.Clients;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Windows.Input;
using Xamarin.Forms;

namespace SejaFlorApp.ViewModels.Clients
{
    public class ClientsListViewModel : ClientListBaseViewModel
    {
        public ICommand NewClientCommand { get; set; }
        public ClientService _clientService { get; set; }

        private Client _client;
        public Client Client
        {
            get
            {
                return _client;
            }
            set
            {
                _client = value;
                OnPropertyChanged("Client");
                if(_client != null)
                {
                    NavigateToDetailsClient(_client.Id);
                    Client = null;
                }
                    
            }
        }

        public ClientsListViewModel()
        {
            NewClientCommand = new Command(NavigateToNewClientPage);
            _clientService = new ClientService();
            GetClients();            
            MessagingCenter.Subscribe<string>(this, "UpdateListClients", (model) =>
            {
                GetClients();
            });
        }

        private async void GetClients()
        {
            IsBusy = true;
            var clients = await _clientService.ListAllAsync();
            Clients = new ObservableCollection<Client>(clients.OrderBy(x => x.Name).ToList());
            OriginClients = Clients;
            IsBusy = false;
        }

        private async void NavigateToNewClientPage()
        {
            await Nav.Navigation.PushAsync(new NewClientPage());
        }        

        private async void NavigateToDetailsClient(int idClient)
        {
            await Nav.Navigation.PushAsync(new DetailsClientPage(idClient));
        }
    }
}
