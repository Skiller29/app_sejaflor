﻿using Acr.UserDialogs;
using SejaFlorApp.Models;
using SejaFlorApp.Services;
using SejaFlorApp.Views.Clients;
using System.Windows.Input;
using Xamarin.Forms;

namespace SejaFlorApp.ViewModels.Clients
{
    public class DetailsClientViewModel : BaseViewModel
    {
        public ICommand SalesCommand { get; set; }
        public ICommand EditCommand { get;set; }
        public ICommand RemoveCommand { get; set; }
        public ClientService _clientService { get; set; }

        private Client _client;
        public Client Client
        {
            get
            {
                return _client;
            }

            set
            {
                _client = value;
                OnPropertyChanged("Client");
            }
        }

        public DetailsClientViewModel(int idClient)
        {
            _clientService = new ClientService();
            this.SalesCommand = new Command(SalesClient);
            this.EditCommand = new Command(EditClient);
            this.RemoveCommand = new Command(RemoveClient);
            LoadClient(idClient);

            MessagingCenter.Subscribe<Client>(this, "UpdateDetailsClient", (model) =>
            {
                LoadClient(model.Id);
            });
        }

        private async void LoadClient(int idClient)
        {
            IsBusy = true;
            var client = await _clientService.LoadAsync(idClient);
            if (client != null)
                Client = client;
            else
                Client = new Client();

            IsBusy = false;
        }

        private async void RemoveClient()
        {
            var p = await UserDialogs.Instance.ConfirmAsync($"Tem certeza que deseja excluír o cliente {Client.Name}?", null, "Sim", "Não");
            if(p)
            {
                IsBusy = true;
                await _clientService.RemoveAsync(Client.Id);
                MessagingCenter.Send("", "UpdateListClients");
                ShowMessage("Cliente removido com sucesso.");
                await Nav.Navigation.PopAsync();
                IsBusy = false;
            }            
        }        

        private async void EditClient()
        {
            await Nav.Navigation.PushAsync(new EditClientPage(Client.Id));
        }

        private async void SalesClient()
        {
            await Nav.Navigation.PushAsync(new SalesClientPage(Client.Id));
        }
    }
}
