﻿using SejaFlorApp.Models;
using SejaFlorApp.Models.Enum;
using SejaFlorApp.Services;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;

namespace SejaFlorApp.ViewModels.Clients
{
    public class EditClientViewModel : BaseViewModel
    {
        public ICommand SaveChangesCommand {get;set;}
        public ClientService _clientService { get; set; }

        private Client _client;
        public Client Client
        {
            get
            {
                return _client;
            }

            set
            {
                _client = value;
                OnPropertyChanged("Client");
            }
        }

        private List<string> _genres;
        public List<string> Genres
        {
            get
            {
                return _genres;
            }

            set
            {
                _genres = value;
                OnPropertyChanged("Genres");
            }
        }

        private string _genre;
        public string Genre
        {
            get
            {
                return _genre;
            }

            set
            {
                _genre = value;
                OnPropertyChanged("Genre");
            }
        }

        public EditClientViewModel(int idClient)
        {
            this.SaveChangesCommand = new Command(SaveChanges);
            Genres = new List<string>() { "Feminino", "Masculino" };            
            _clientService = new ClientService();
            LoadClient(idClient);
        }

        private async void LoadClient(int idClient)
        {
            IsBusy = true;
            var client = await _clientService.LoadAsync(idClient);
            if (client.Genre == Models.Enum.Genre.Female)
                Genre = "Feminino";
            else
                Genre = "Masculino";

            if (client != null)
                Client = client;
            else
                Client = new Client();

            IsBusy = false;
        }

        private async void SaveChanges()
        {
            IsBusy = true;
            try
            {
                Client.Genre = GetGenre();
                await _clientService.EditAsync(Client);
                ShowMessage("Alterações salvas com sucesso.");
                MessagingCenter.Send(Client, "UpdateDetailsClient");
                MessagingCenter.Send("", "UpdateListClients");
                await Nav.Navigation.PopAsync();
            }
            catch (System.Exception ex)
            {
                ShowMessage(ex.Message);                
            }

            IsBusy = false;
        }

        private Genre GetGenre()
        {
            if (Genre == "Masculino")
                return Models.Enum.Genre.Male;


            return Models.Enum.Genre.Female;
        }
    }
}
