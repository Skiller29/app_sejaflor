﻿using SejaFlorApp.Models;
using SejaFlorApp.Models.Enum;
using SejaFlorApp.Services;
using System.Collections.Generic;
using System.Windows.Input;
using Xamarin.Forms;

namespace SejaFlorApp.ViewModels.Clients
{
    public class NewClientViewModel : BaseViewModel
    {
        public ICommand AddClientCommand { get; set; }

        public ClientService _clientService { get; set; }

        private Client _client;
        public Client Client
        {
            get
            {
                return _client;
            }

            set
            {
                _client = value;
                OnPropertyChanged("Client");
            }
        }

        private List<string> _genres;
        public List<string> Genres
        {
            get
            {
                return _genres;
            }

            set
            {
                _genres = value;
                OnPropertyChanged("Genres");
            }
        }

        private string _genre;
        public string Genre
        {
            get
            {
                return _genre;
            }

            set
            {
                _genre = value;
                OnPropertyChanged("Genre");
            }
        }

        public NewClientViewModel()
        {
            this.AddClientCommand = new Command(AddClient);
            Genres = new List<string>() { "Feminino", "Masculino" };
            Client = new Client();
            Genre = "Feminino";
            _clientService = new ClientService();
        }

        private async void AddClient()
        {
            IsBusy = true;
            try
            {
                Client.Genre = GetGenre();
                await _clientService.InsertAsync(Client);
                ShowMessage("Cliente cadastrado com sucesso.");
                MessagingCenter.Send("", "UpdateListClients");
                await Nav.Navigation.PopAsync();
            }
            catch (System.Exception ex)
            {
                ShowMessage(ex.Message);               
            }
            IsBusy = false;
        }

        private Genre GetGenre()
        {
            if (Genre == "Masculino")
                return Models.Enum.Genre.Male;


            return Models.Enum.Genre.Female;
        }
    }
}
