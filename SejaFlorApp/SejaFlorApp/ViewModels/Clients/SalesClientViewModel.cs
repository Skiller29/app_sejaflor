﻿using SejaFlorApp.Models;
using SejaFlorApp.Services;
using SejaFlorApp.Views.Sales;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using Xamarin.Forms;

namespace SejaFlorApp.ViewModels.Clients
{
    public class SalesClientViewModel : BaseViewModel
    {
        private readonly SaleService _saleService;

        private ObservableCollection<Sale> _sales;
        public ObservableCollection<Sale> Sales
        {
            get
            {
                return _sales;
            }
            set
            {
                _sales = value;
                OnPropertyChanged("Sales");
            }
        }

        private Sale _sale;
        public Sale Sale
        {
            get
            {
                return _sale;
            }
            set
            {
                _sale = value;
                OnPropertyChanged("Sale");
                if(_sale != null)
                {
                    NavigateToDetailsSale(_sale);
                    Sale = null;
                }
            }
        }

        public SalesClientViewModel(int idClient)
        {
            _saleService = new SaleService();
            GetSales(idClient);

            MessagingCenter.Subscribe<string>(this, "UpdateListSales", (model) =>
            {
                GetSales(idClient);
            });
        }

        private async void GetSales(int idClient)
        {
            var sales = await _saleService.ListAsync(x => x.ClientID == idClient);
            if (sales != null)
                Sales = new ObservableCollection<Sale>(sales.OrderByDescending(x => x.Id).ToList());
            else
                Sales = new ObservableCollection<Sale>();
        }

        private async void NavigateToDetailsSale(Sale sale)
        {
            await Nav.Navigation.PushAsync(new DetailsSalePage(sale));
        }
    }
}
