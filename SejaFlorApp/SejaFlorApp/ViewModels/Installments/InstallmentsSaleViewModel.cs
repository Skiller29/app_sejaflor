﻿using Rg.Plugins.Popup.Extensions;
using SejaFlorApp.Models;
using SejaFlorApp.Services;
using SejaFlorApp.Views.Installments;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using System.Windows.Input;
using Xamarin.Forms;

namespace SejaFlorApp.ViewModels.Installments
{
    public class InstallmentsSaleViewModel : BaseViewModel
    {
        public ICommand PayCommand { get; set; }
        private readonly SaleService _saleService;

        private ObservableCollection<Installment> _installments;
        public ObservableCollection<Installment> Installments
        {
            get
            {
                return _installments;
            }
            set
            {
                _installments = value;
                OnPropertyChanged("Installments");
            }
        }

        public InstallmentsSaleViewModel(int idSale)
        {
            this.PayCommand = new Command<Installment>(PayInstallment);
            _saleService = new SaleService();
            GetInstallments(idSale);

            MessagingCenter.Subscribe<string>(this, "UpdateInstallments", (model) =>
            {
                GetInstallments(int.Parse(model));
            });
        }

        private async void GetInstallments(int idSale)
        {
            IsBusy = true;
            var installments = await _saleService.GetInstallmentsSaleAsync(idSale);
            if (installments != null)
                Installments = new ObservableCollection<Installment>(installments);
            else
                Installments = new ObservableCollection<Installment>();

            IsBusy = false;
        }

        private async void PayInstallment(Installment installment)
        {
            IsBusy = true;
            var canPay = await _saleService.CanPayInstallmentAsync(installment.Id);
            if (!canPay)
            {
                ShowMessage("Existem parcelas anteriores em aberto.");
                IsBusy = false;
                return;
            }

            if (!installment.IsPaid)
                await Nav.Navigation.PushPopupAsync(new PayInstallmentPage(installment));

            IsBusy = false;
        }
    }
}
