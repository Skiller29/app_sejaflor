﻿using Acr.UserDialogs;
using Rg.Plugins.Popup.Extensions;
using SejaFlorApp.Models;
using SejaFlorApp.Services;
using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Input;
using Xamarin.Forms;

namespace SejaFlorApp.ViewModels.Installments
{
    public class PayInstallmentViewModel : BaseViewModel
    {
        public ICommand PayCommand { get; set; }
        private readonly SaleService _saleService;

        private Installment _installment;
        public Installment Installment
        {
            get
            {
                return _installment;
            }
            set
            {
                _installment = value;
                OnPropertyChanged("Installment");
            }
        }

        private decimal _valuePaid;
        public decimal ValuePaid
        {
            get
            {
                return _valuePaid;
            }
            set
            {
                _valuePaid = value;
                OnPropertyChanged("ValuePaid");
            }
        }

        public PayInstallmentViewModel(Installment installment)
        {
            Installment = installment;
            Installment.PaymentDate = DateTime.Now;
            ValuePaid = installment.Value;
            _saleService = new SaleService();
            this.PayCommand = new Command(Pay);
        }

        private async void Pay()
        {
            if(Installment.PaymentDate == DateTime.MinValue)
            {
                ShowMessage("Por favor, escolha a data que o pagamento foi efetuado.");
                return;
            }
         
            if(ValuePaid != Installment.Value)
            {
                if (await _saleService.IsLastInstallmentAsync(Installment.Id, Installment.SaleID))
                {
                    if(ValuePaid > Installment.Value)
                    {
                        ShowMessage("Esta é a última parcela. O valor recebido não pode ser maior que o valor da parcela.");
                        return;
                    }
                    else if(ValuePaid < Installment.Value)
                    {
                        var op = await UserDialogs.Instance.ConfirmAsync("O Valor recebido é menor que o valor da parcela. Deseja criar uma nova parcela com o valor que está faltando?", null, "Sim", "Não");
                        if (op)
                        {
                            var newInstallment = new Installment()
                            {
                                DueDate = Installment.DueDate.AddMonths(1),
                                NumberInstallment = Installment.Sale.NumberInstallments + 1,
                                SaleID = Installment.Sale.Id,
                                Value = Installment.Value - ValuePaid,
                                IsPaid = false
                            };
                            await _saleService.InsertNewInstallmentAsync(newInstallment);
                        }
                        else
                            return;
                    }
                }
                else
                {
                    if(ValuePaid > Installment.Value)
                    {
                        var op = await UserDialogs.Instance.ConfirmAsync("O Valor recebido é maior do que o valor da parcela. Deseja abater o valor a mais na próxima parcela?", null, "Sim", "Não");
                        if (op)
                        {
                            await _saleService.UpdateInstallmentAsync(ValuePaid - Installment.Value, Installment.Id, Installment.SaleID, true);
                        }
                        else
                            return;

                    }
                    else if(ValuePaid < Installment.Value)
                    {
                        var op = await UserDialogs.Instance.ConfirmAsync("O Valor recebido é menor do que o valor da parcela. Deseja passar o valor que falta para a próxima parcela?", null, "Sim", "Não");
                        if (op)
                        {
                            await _saleService.UpdateInstallmentAsync(Installment.Value - ValuePaid, Installment.Id, Installment.SaleID, false);
                        }
                        else
                            return;
                    }
                }
            }
            ShowMessage("Parcela paga com sucesso.");
            Installment.IsPaid = true;
            Installment.Value = ValuePaid;
            await _saleService.PayInstallmentAsync(Installment);
            MessagingCenter.Send(Installment.SaleID.ToString(), "UpdateInstallments");
            await Nav.Navigation.PopPopupAsync();
        }
    }
}
