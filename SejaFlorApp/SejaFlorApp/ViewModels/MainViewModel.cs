﻿using SejaFlorApp.Views.Clients;
using SejaFlorApp.Views.Products;
using SejaFlorApp.Views.Sales;
using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Input;
using Xamarin.Forms;

namespace SejaFlorApp.ViewModels
{
    public class MainViewModel : BaseViewModel
    {
        public ICommand NewClientCommand { get; set; }
        public ICommand NewProductCommand { get; set; }
        public ICommand NewSaleCommand { get; set; }

        public MainViewModel()
        {
            this.NewClientCommand = new Command(NewClient);
            this.NewProductCommand = new Command(NewProduct);
            this.NewSaleCommand = new Command(NewSale);
        }

        private async void NewClient()
        {
            await Nav.Navigation.PushAsync(new NewClientPage());
        }

        private async void NewProduct()
        {
            await Nav.Navigation.PushAsync(new NewProductPage());
        }

        private async void NewSale()
        {
            await Nav.Navigation.PushAsync(new NewSalePage());
        }
    }
}
