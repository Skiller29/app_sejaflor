﻿using SejaFlorApp.Views;
using SejaFlorApp.Views.Clients;
using SejaFlorApp.Views.Products;
using SejaFlorApp.Views.Reports;
using SejaFlorApp.Views.Sales;
using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;

namespace SejaFlorApp.ViewModels
{
    public class MasterViewModel : BaseViewModel
    {
        private Item _optionSelected;
        public Item OptionSelected
        {
            get { return _optionSelected; }
            set {
                _optionSelected = value;
                OnPropertyChanged("OptionSelected");
                if(OptionSelected != null)
                {
                    NavigateToOption(_optionSelected);
                    OptionSelected = null;
                }
                
            }
        }

        
        private async void NavigateToOption(Item option)
        {
            var masterPage = (MasterDetailPage)App.Current.MainPage;
            masterPage.IsPresented = false;
            switch (option.Option)
            {
                case "Home":
                    break;
                case "Vendas":
                    await Nav.Navigation.PushAsync(new SalesListPage());
                    break;
                case "Clientes":
                    await Nav.Navigation.PushAsync(new ClientsListPage());
                    break;
                case "Produtos":
                    await Nav.Navigation.PushAsync(new ProductsListPage());
                    break;
                case "Relatório":
                    await Nav.Navigation.PushAsync(new MonthlyReportPage());
                    break;
                case "Configurações":
                    await Nav.Navigation.PushAsync(new SettingsPage());
                    break;
                default:
                    break;
            }
        }
    }
}
