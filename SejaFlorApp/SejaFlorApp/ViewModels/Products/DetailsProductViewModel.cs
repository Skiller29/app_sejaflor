﻿using Acr.UserDialogs;
using SejaFlorApp.Models;
using SejaFlorApp.Services;
using SejaFlorApp.Views.Products;
using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Input;
using Xamarin.Forms;

namespace SejaFlorApp.ViewModels.Products
{
    public class DetailsProductViewModel : BaseViewModel
    {
        public ICommand EditCommand { get; set; }
        public ICommand RemoveCommand { get; set; }
        public ProductService _productService { get; set; }

        private Product _product;
        public Product Product
        {
            get
            {
                return _product;
            }
            set
            {
                _product = value;
                OnPropertyChanged("Product");
            }
        }

        public DetailsProductViewModel(int idProduct)
        {
            _productService = new ProductService();
            this.EditCommand = new Command(EditProduct);
            this.RemoveCommand = new Command(RemoveProduct);
            LoadProduct(idProduct);

            MessagingCenter.Subscribe<Product>(this, "UpdateDetailsProduct", (model) =>
            {
                LoadProduct(model.Id);
            });
        }

        private async void LoadProduct(int idProduct)
        {
            IsBusy = true;
            var product = await _productService.LoadAsync(idProduct);
            if (product != null)
                Product = product;
            else
                Product = new Product();

            IsBusy = false;
        }

        private async void RemoveProduct()
        {
            var p = await UserDialogs.Instance.ConfirmAsync($"Tem certeza que deseja excluír o produto {Product.Name}", null, "Sim", "Não");
            if (p)
            {
                await _productService.RemoveAsync(Product.Id);
                MessagingCenter.Send("", "UpdateListProducts");
                ShowMessage("Produto removido com sucesso.");
                await Nav.Navigation.PopAsync();
            }
        }

        private async void EditProduct()
        {
            await Nav.Navigation.PushAsync(new EditProductPage(Product.Id));
        }
    }
}
