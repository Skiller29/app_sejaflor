﻿using SejaFlorApp.Models;
using SejaFlorApp.Services;
using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Input;
using Xamarin.Forms;

namespace SejaFlorApp.ViewModels.Products
{
    public class EditProductViewModel : BaseViewModel
    {
        public ICommand SaveChangesCommand { get; set; }
        public ProductService _productService { get; set; }

        private Product _product;
        public Product Product
        {
            get
            {
                return _product;
            }
            set
            {
                _product = value;
                OnPropertyChanged("Product");
            }
        }

        public EditProductViewModel(int idProduct)
        {
            _productService = new ProductService();
            this.SaveChangesCommand = new Command(SaveChanges);
            LoadClient(idProduct);
        }

        private async void LoadClient(int idProduct)
        {
            IsBusy = true;
            var product = await _productService.LoadAsync(idProduct);           
            if (product != null)
                Product = product;
            else
                Product = new Product();

            IsBusy = false;
        }

        private async void SaveChanges()
        {
            IsBusy = true;
            try
            {                
                await _productService.EditAsync(Product);
                ShowMessage("Alterações salvas com sucesso.");
                MessagingCenter.Send(Product, "UpdateDetailsProduct");
                MessagingCenter.Send("", "UpdateListProducts");
                await Nav.Navigation.PopAsync();
            }
            catch (System.Exception ex)
            {
                ShowMessage(ex.Message);
            }
            IsBusy = false;
        }
    }
}
