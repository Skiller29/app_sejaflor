﻿using SejaFlorApp.Models;
using SejaFlorApp.Services;
using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Input;
using Xamarin.Forms;

namespace SejaFlorApp.ViewModels.Products
{
    public class NewProductViewModel : BaseViewModel
    {
        public ICommand AddCommand { get; set; }
        public ProductService _productService { get; set; }

        private Product _product;
        public Product Product
        {
            get
            {
                return _product;
            }
            set
            {
                _product = value;
                OnPropertyChanged("Product");                                    
            }
        }

        public NewProductViewModel()
        {
            _productService = new ProductService();
            this.AddCommand = new Command(AddProduct);
            Product = new Product();
        }

        private async void AddProduct()
        {
            IsBusy = true;
            try
            {                
                await _productService.InsertAsync(Product);
                ShowMessage("Produto cadastrado com sucesso.");
                MessagingCenter.Send("", "UpdateListProducts");
                await Nav.Navigation.PopAsync();
            }
            catch (System.Exception ex)
            {
                ShowMessage(ex.Message);
            }
            IsBusy = false;
        }
    }
}
