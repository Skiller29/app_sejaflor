﻿using SejaFlorApp.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;

namespace SejaFlorApp.ViewModels.Products
{
    public class ProductsListBaseViewModel : BaseViewModel
    {
        private string _search;
        public string Search
        {
            get
            {
                return _search;
            }

            set
            {
                _search = value;
                OnPropertyChanged("Search");
                FilterProducts(_search);
            }
        }

        private ObservableCollection<Product> _products;
        public ObservableCollection<Product> Products
        {
            get
            {
                return _products;
            }

            set
            {
                _products = value;
                OnPropertyChanged("Products");
            }
        }        

        public ObservableCollection<Product> OriginProduts { get; set; }


        private void FilterProducts(string op)
        {
            if (!string.IsNullOrEmpty(op))
            {
                Products = new ObservableCollection<Product>(OriginProduts.Where(x => x.Name.ToLower().Contains(op.ToLower())).OrderBy(x => x.Name).ToList());
            }
            else
            {
                Products = OriginProduts;
            }
        }
    }
}
