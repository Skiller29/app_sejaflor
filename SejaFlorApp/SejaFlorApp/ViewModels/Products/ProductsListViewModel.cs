﻿using SejaFlorApp.Models;
using SejaFlorApp.Services;
using SejaFlorApp.Views.Products;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows.Input;
using Xamarin.Forms;

namespace SejaFlorApp.ViewModels.Products
{
    public class ProductsListViewModel : ProductsListBaseViewModel
    {
        public ICommand NewProductCommand { get; set; }
        public ProductService _productService { get; set; }

        private Product _product;
        public Product Product
        {
            get
            {
                return _product;
            }
            set
            {
                _product = value;
                OnPropertyChanged("Product");
                if (_product != null)
                {
                    NavigateToDetailsProduct(_product.Id);
                    Product = null;
                }
                    
            }
        }

        public ProductsListViewModel()
        {
            _productService = new ProductService();
            NewProductCommand = new Command(NavigateToNewProductPage);
            GetProducts();
            
            MessagingCenter.Subscribe<string>(this, "UpdateListProducts", (model) =>
            {
                GetProducts();
            });
        }

        private async void GetProducts()
        {
            IsBusy = true;
            var products = await _productService.ListAllAsync();
            Products = new ObservableCollection<Product>(products.OrderBy(x => x.Name).ToList());
            OriginProduts = Products;
            IsBusy = false;
        }

        private async void NavigateToNewProductPage()
        {
            await Nav.Navigation.PushAsync(new NewProductPage());
        }

        private async void NavigateToDetailsProduct(int idProduct)
        {
            await Nav.Navigation.PushAsync(new DetailsProductPage(idProduct));
        }
    }
}
