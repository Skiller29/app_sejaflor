﻿using SejaFlorApp.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SejaFlorApp.ViewModels.Reports
{
    public class GeneralReportViewModel : BaseViewModel
    {
        private string _total;
        public string Total
        {
            get { return _total; }
            set
            {
                _total = value;
                OnPropertyChanged("Total");
            }
        }

        private string _totalSold;
        public string TotalSold
        {
            get { return _totalSold; }
            set
            {
                _totalSold = value;
                OnPropertyChanged("TotalSold");
            }
        }

        private string _profitSold;
        public string ProfitSold
        {
            get { return _profitSold; }
            set
            {
                _profitSold = value;
                OnPropertyChanged("ProfitSold");
            }
        }

        private string _totalReceived;
        public string TotalReceived
        {
            get { return _totalReceived; }
            set
            {
                _totalReceived = value;
                OnPropertyChanged("TotalReceived");
            }
        }

        private string _profitReceived;
        public string ProfitReceived
        {
            get { return _profitReceived; }
            set
            {
                _profitReceived = value;
                OnPropertyChanged("ProfitReceived");
            }
        }

        private List<Installment> _installmentsPaid;
        public List<Installment> InstallmentsPaid
        {
            get { return _installmentsPaid; }
            set
            {
                _installmentsPaid = value;
                OnPropertyChanged("InstallmentsPaid");
            }
        }

        private List<Installment> _installmentsNotPaid;
        public List<Installment> InstallmentsNotPaid
        {
            get { return _installmentsNotPaid; }
            set
            {
                _installmentsNotPaid = value;
                OnPropertyChanged("InstallmentsNotPaid");
            }
        }

        public GeneralReportViewModel(List<Installment> installments, string total, string totalReceived, string profitReceived, string totalSold, string profitSold)
        {
            if(installments != null && installments.Any())
            {
                InstallmentsPaid = installments.Where(x => x.IsPaid).ToList();
                InstallmentsNotPaid = installments.Where(x => !x.IsPaid).ToList();
                Total = total;
                TotalReceived = totalReceived;
                TotalSold = totalSold;
                ProfitReceived = profitReceived;
                ProfitSold = profitSold;
            }
        }
    }
}
