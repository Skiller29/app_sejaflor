﻿using SejaFlorApp.Models;
using SejaFlorApp.Services;
using SejaFlorApp.Views.Reports;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Input;
using Xamarin.Forms;

namespace SejaFlorApp.ViewModels.Reports
{
    public class MonthlyReportViewModel : BaseViewModel
    {
        public ICommand FilterCommand { get; set; }
        private readonly SaleService _saleService;

        private DateTime _initialDate;
        public DateTime InitialDate
        {
            get { return _initialDate; }
            set
            {
                _initialDate = value;
                OnPropertyChanged("InitialDate");                
            }
        }

        private DateTime _endDate;
        public DateTime EndDate
        {
            get { return _endDate; }
            set
            {
                _endDate = value;
                OnPropertyChanged("EndDate");
            }
        }
        
        public MonthlyReportViewModel()
        {
            this.FilterCommand = new Command(Filter);
            InitialDate = DateTime.Now;
            EndDate = DateTime.Now.AddDays(7);
            _saleService = new SaleService();
        }

        private async void Filter()
        {
            string totalString, totalReceivedString, profitReceivedString, totalSoldString, profitSoldString;
            if (EndDate < InitialDate)
            {
                ShowMessage("A data de início deve ser maior que a data final.");
                return;
            }

            var sales = await _saleService.FilterSalesAsync(InitialDate, EndDate);
            totalString = string.Format("R$ {0:N}", sales.Sum(x=>x.Total));

            var installments = await _saleService.FilterInstallmentsAsync(InitialDate, EndDate);
            if (installments != null && installments.Any())
            {                                                
                //CALCULING TOTAL
                var totalSold = installments.Sum(x => x.Value);
                totalSoldString =  string.Format("R$ {0:N}", totalSold);

                decimal profitSold = 0;
                foreach (var item in installments)
                {
                    var sale = await _saleService.LoadAsync(item.SaleID);
                    if(sale != null)
                    {
                        var percentageSold = decimal.Round(item.Value / sale.Total, 2);
                        var finalProfit = sale.Profit * percentageSold;
                        profitSold += finalProfit;
                    }                    
                }
                profitSoldString = string.Format("R$ {0:N}", profitSold);

                //CALCULING TOTAL RECEIVED
                var totalReceived = installments.Where(x => x.IsPaid).Sum(x => x.Value);
                totalReceivedString = string.Format("R$ {0:N}", totalReceived);

                decimal profitReceived = 0;
                foreach (var item in installments.Where(x=>x.IsPaid))
                {
                    var sale = await _saleService.LoadAsync(item.SaleID);
                    if (sale != null)
                    {
                        var percentageReceived = item.Value / sale.Total;
                        var finalReceived = sale.Profit * percentageReceived;
                        profitReceived += finalReceived;
                    }
                }
                profitReceivedString = string.Format("R$ {0:N}", profitReceived);
                await Nav.Navigation.PushAsync(new GeneralReportPage(installments, totalString, totalReceivedString, profitReceivedString, totalSoldString, profitSoldString));
            }            
        }
    }
}
