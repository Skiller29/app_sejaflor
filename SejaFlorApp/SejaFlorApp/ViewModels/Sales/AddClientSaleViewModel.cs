﻿using SejaFlorApp.Models;
using SejaFlorApp.Services;
using SejaFlorApp.ViewModels.Clients;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Windows.Input;
using Xamarin.Forms;

namespace SejaFlorApp.ViewModels.Sales
{
    public class AddClientSaleViewModel : ClientListBaseViewModel
    {
        public ClientService _clientService { get; set; }

        private Client _client;
        public Client Client
        {
            get
            {
                return _client;
            }

            set
            {
                _client = value;
                AddClient(_client);
                OnPropertyChanged("Client");
            }
        }

        public AddClientSaleViewModel()
        {
            _clientService = new ClientService();            
            OriginClients = Clients;
            LoadClients();
        }

        private async void AddClient(Client client)
        {
            if (client == null)
            {
                ShowMessage("Por favor, selecione um cliente.");
                return;
            }

            MessagingCenter.Send(client, "AddClientSale");
            await Nav.Navigation.PopAsync();
        }

        private async void LoadClients()
        {
            IsBusy = true;
            var clients = await _clientService.ListAllAsync();
            Clients = new ObservableCollection<Client>(clients.OrderBy(x => x.Name).ToList());
            OriginClients = Clients;
            IsBusy = false;
        }
    }
}
