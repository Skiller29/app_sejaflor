﻿using SejaFlorApp.Models;
using SejaFlorApp.Services;
using SejaFlorApp.ViewModels.Products;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Windows.Input;
using Xamarin.Forms;

namespace SejaFlorApp.ViewModels.Sales
{
    public class AddProductSaleViewModel : ProductsListBaseViewModel
    {
        public ProductService _productService { get; set; }

        private Product _product;
        public Product Product
        {
            get
            {
                return _product;
            }
            set
            {
                _product = value;
                AddProduct(_product);
                OnPropertyChanged("Product");
            }
        }

        public AddProductSaleViewModel()
        {
            _productService = new ProductService();            
            LoadProducts();                        
        }

        private async void AddProduct(Product product)
        {
            if (product == null)
            {
                ShowMessage("Por favor, selecione um produto.");
                return;
            }

            var itemSale = new ItemSale() { Product = product, ProductID = product.Id, Quantity = 1 };
            MessagingCenter.Send(itemSale, "AddProductSale");
            await Nav.Navigation.PopAsync();
        }

        private async void LoadProducts()
        {
            IsBusy = true;
            var products = await _productService.ListAllAsync();
            Products = new ObservableCollection<Product>(products.OrderBy(x => x.Name).ToList());
            OriginProduts = Products;
            IsBusy = false;
        }
    }
}
