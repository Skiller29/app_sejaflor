﻿using Acr.UserDialogs;
using SejaFlorApp.Models;
using SejaFlorApp.Services;
using SejaFlorApp.Views.Installments;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Input;
using Xamarin.Forms;

namespace SejaFlorApp.ViewModels.Sales
{
    public class DetailsSaleViewModel : BaseViewModel
    {
        public ICommand RemoveCommand { get; set; }
        public ICommand InstallmentsCommand { get; set; }
        private readonly SaleService _saleService;
        private readonly ProductService _productService;

        private Sale _sale;
        public Sale Sale
        {
            get
            {
                return _sale;
            }

            set
            {
                _sale = value;
                OnPropertyChanged("Sale");
            }
        }

        private List<ItemSale> _itemSales;
        public List<ItemSale> ItemSales
        {
            get
            {
                return _itemSales;
            }

            set
            {
                _itemSales = value;
                OnPropertyChanged("ItemSales");
            }
        }

        private string _installments;
        public string Installments
        {
            get
            {
                return _installments;
            }

            set
            {
                _installments = value;
                OnPropertyChanged("Installments");
            }
        }

        private string _typePayment;
        public string TypePayment
        {
            get
            {
                return _typePayment;
            }

            set
            {
                _typePayment = value;
                OnPropertyChanged("TypePayment");
            }
        }

        public DetailsSaleViewModel(Sale sale)
        {
            _saleService = new SaleService();
            _productService = new ProductService();
            this.RemoveCommand = new Command(RemoveSale);
            this.InstallmentsCommand = new Command(DetailsInstallments);
            Sale = sale;
            GetDataSale();
        }

        private async void GetDataSale()
        {
            //GET ITEMS SALE
            ItemSales = await _saleService.GetItemsSaleAsync(Sale.Id);
            
            switch (Sale.TypePayment)
            {
                case Models.Enum.TypePayment.Cash:
                    TypePayment = "Dinheiro";
                    break;
                case Models.Enum.TypePayment.CreditCard:
                    TypePayment = "Cartão de crédito";
                    break;
                case Models.Enum.TypePayment.DebitCard:
                    TypePayment = "Cartão de débito";
                    break;
                case Models.Enum.TypePayment.Financing:
                    TypePayment = "Á prazo";
                    break;
                default:
                    TypePayment = "";
                    break;
            }

            var total = 0M;
            if (Sale.ItemsSale != null)
            {
                foreach (var prod in Sale.ItemsSale)
                    total = total + prod.Amount * prod.Quantity;

                var installment = total / Sale.NumberInstallments;
                Installments = string.Format("{0}x R$ {1:N}", Sale.NumberInstallments, installment);
            }
        }

        private async void RemoveSale()
        {
            var p = await UserDialogs.Instance.ConfirmAsync($"Tem certeza que deseja excluír esta venda?", null, "Sim", "Não");
            if (p)
            {
                IsBusy = true;
                await _saleService.RemoveAsync(Sale.Id);
                MessagingCenter.Send("", "UpdateListSales");
                ShowMessage("Venda removida com sucesso.");
                await Nav.Navigation.PopAsync();
                IsBusy = false;
            }
        }

        private async void DetailsInstallments()
        {
            await Nav.Navigation.PushAsync(new InstallmentsSalePage(Sale.Id));
        }
    }
}
