﻿using SejaFlorApp.Models;
using SejaFlorApp.Services;
using SejaFlorApp.Views.Sales;
using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Input;
using Xamarin.Forms;

namespace SejaFlorApp.ViewModels.Sales
{
    public class FinishNewSaleViewModel : BaseViewModel
    {
        public ICommand FinishCommand { get; set; }
        public SaleService _saleService { get; set; }

        private Sale _sale;
        public Sale Sale
        {
            get
            {
                return _sale;
            }

            set
            {
                _sale = value;
                OnPropertyChanged("Sale");
            }
        }

        private string _installments;
        public string Installments
        {
            get
            {
                return _installments;
            }

            set
            {
                _installments = value;
                OnPropertyChanged("Installments");
            }
        }

        private string _typePayment;
        public string TypePayment
        {
            get
            {
                return _typePayment;
            }

            set
            {
                _typePayment = value;
                OnPropertyChanged("TypePayment");
            }
        }

        public List<ItemSale> Products { get; set; }

        public FinishNewSaleViewModel(Sale sale, List<ItemSale> products)
        {
            Products = products;
            _saleService = new SaleService();
            this.FinishCommand = new Command(FinishSale);
            Sale = sale;
            CalculateTotal();
            GenerateTypePayment();
        }

        private void CalculateTotal()
        {
            var total = 0M;
            var totalCost = 0M;
            foreach (var prod in Products)
            {
                total = total + (Sale.TypePayment == Models.Enum.TypePayment.Cash ? prod.Product.SaleAmountInCash : prod.Product.SaleAmountInCard) * prod.Quantity;
                totalCost = totalCost + prod.Product.CostAmount * prod.Quantity;
            }

            var installment = total / Sale.NumberInstallments;
            Installments = string.Format("{0}x R$ {1:N}", Sale.NumberInstallments, installment);
            Sale.Total = total;
            Sale.TotalCost = totalCost;
        }

        private void GenerateTypePayment()
        {
            switch (Sale.TypePayment)
            {
                case Models.Enum.TypePayment.Cash:
                    TypePayment = "Dinheiro";
                    break;
                case Models.Enum.TypePayment.CreditCard:
                    TypePayment = "Cartão de crédito";
                    break;
                case Models.Enum.TypePayment.DebitCard:
                    TypePayment = "Cartão de débito";
                    break;
                case Models.Enum.TypePayment.Financing:
                    TypePayment = "Á prazo";
                    break;
                default:
                    TypePayment = "";
                    break;
            }
        }

        private async void FinishSale()
        {
            IsBusy = true;
            try
            {                
                await _saleService.InsertAsync(Sale, Products);
                ShowMessage("Venda finalizada com sucesso.");
                MessagingCenter.Send("", "UpdateListSales");
                await Nav.Navigation.PopToRootAsync();
                await Nav.Navigation.PushAsync(new SalesListPage());
            }
            catch (Exception ex)
            {
                ShowMessage(ex.Message);
            }
            IsBusy = false;
        }
    }
}
