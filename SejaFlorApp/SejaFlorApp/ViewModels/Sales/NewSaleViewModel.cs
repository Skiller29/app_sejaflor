﻿using SejaFlorApp.Models;
using SejaFlorApp.Views.Sales;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Windows.Input;
using Xamarin.Forms;

namespace SejaFlorApp.ViewModels.Sales
{
    public class NewSaleViewModel : BaseViewModel
    {
        public ICommand AddClientCommand { get; set; }
        public ICommand AddProductCommand { get; set; }
        public ICommand RemoveProductCommand { get; set; }
        public ICommand ContinueCommand { get; set; }
       
        private Client _client;
        public Client Client
        {
            get
            {
                return _client;
            }

            set
            {
                _client = value;
                OnPropertyChanged("Client");
            }
        }

        private ObservableCollection<ItemSale> _products;
        public ObservableCollection<ItemSale> Products
        {
            get
            {
                return _products;
            }

            set
            {
                _products = value;
                OnPropertyChanged("Products");
            }
        }

        public NewSaleViewModel()
        {
            this.AddClientCommand = new Command(AddClient);
            this.AddProductCommand = new Command(AddProduct);
            this.RemoveProductCommand = new Command<ItemSale>(RemoveProduct);
            this.ContinueCommand = new Command(Continue);
            Client = new Client() { Name = "Adicionar cliente", Genre = Models.Enum.Genre.Female };

            MessagingCenter.Subscribe<Client>(this, "AddClientSale", (model) =>
            {
                Client = model;
            });

            MessagingCenter.Subscribe<ItemSale>(this, "AddProductSale", (model) =>
            {
                if (Products == null)
                    Products = new ObservableCollection<ItemSale>();

                Products.Add(model);
            });
        }

        private async void AddClient()
        {
            await Nav.Navigation.PushAsync(new AddClientSalePage());
        }

        private async void AddProduct()
        {
            await Nav.Navigation.PushAsync(new AddProductSalePage());
        }

        private void RemoveProduct(ItemSale model)
        {
            Products.Remove(model);
        }

        private async void Continue()
        {
            if(Client.Name == "Adicionar cliente")
            {
                ShowMessage("Por favor, escolha um cliente para a venda.");
                return;
            }

            if(Products == null || !Products.Any())
            {
                ShowMessage("Por favor, escolha no mínimo um produto para a venda.");
                return;
            }

            if(Products.Any(x=>x.Quantity < 1))
            {
                ShowMessage("A quantidade do produto deve ser maior que 0.");
                return;
            }

            var sale = new Sale()
            {
                Client = Client,
                ClientID = Client.Id
            };

            await Nav.Navigation.PushAsync(new PaymentNewSalePage(sale, Products.ToList()));
        }
    }
}
