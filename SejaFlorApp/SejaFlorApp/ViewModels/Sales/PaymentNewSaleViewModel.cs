﻿using SejaFlorApp.Models;
using SejaFlorApp.Models.Enum;
using SejaFlorApp.Views.Sales;
using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Input;
using Xamarin.Forms;

namespace SejaFlorApp.ViewModels.Sales
{
    public class PaymentNewSaleViewModel : BaseViewModel
    {
        public ICommand ContinueCommand { get; set; }

        private Sale _sale;
        public Sale Sale
        {
            get
            {
                return _sale;
            }

            set
            {
                _sale = value;
                OnPropertyChanged("Sale");
            }
        }

        private DateTime _saleDate;
        public DateTime SaleDate
        {
            get
            {
                return _saleDate;
            }

            set
            {
                _saleDate = value;
                OnPropertyChanged("SaleDate");
            }
        }

        private string _typePayment;
        public string TypePayment
        {
            get
            {
                return _typePayment;
            }

            set
            {
                _typePayment = value;
                OnPropertyChanged("TypePayment");
            }
        }

        private List<string> _typePayments;
        public List<string> TypePayments
        {
            get
            {
                return _typePayments;
            }

            set
            {
                _typePayments = value;
                OnPropertyChanged("TypePayments");
            }
        }

        private bool _isInstallment;
        public bool IsInstallment
        {
            get
            {
                return _isInstallment;
            }

            set
            {
                _isInstallment = value;
                OnPropertyChanged("TypePayment");
            }
        }

        private DateTime _dueDate;
        public DateTime DueDate
        {
            get
            {
                return _dueDate;
            }

            set
            {
                _dueDate = value;
                OnPropertyChanged("DueDate");
            }
        }

        private int _numberInstallments;
        public int NumberInstallments
        {
            get
            {
                return _numberInstallments;
            }

            set
            {
                _numberInstallments = value;
                OnPropertyChanged("NumberInstallments");
            }
        }

        public List<ItemSale> Products { get; set; }

        public PaymentNewSaleViewModel(Sale sale, List<ItemSale> products)
        {
            SaleDate = DateTime.Now;
            DueDate = DateTime.Now;
            NumberInstallments = 1;
            Sale = sale;
            Products = products;
            TypePayments = new List<string>() { "Dinheiro", "Cartão de crédito", "Cartão de débito", "Á prazo" };
            this.ContinueCommand = new Command(Continue);
        }

        private async void Continue()
        {
            if(SaleDate == DateTime.MinValue)
            {
                ShowMessage("Por favor, escolha a data da venda.");
                return;
            }

            if (string.IsNullOrEmpty(TypePayment))
            {
                ShowMessage("Por favor, escolha o tipo de pagamento.");
                return;
            }

            if(IsInstallment)
            {
                if(DueDate == DateTime.MinValue)
                {
                    ShowMessage("Por favor, escolha a data de pagamento das parcelas.");
                    return;
                }

                if(NumberInstallments < 1)
                {
                    ShowMessage("O número de parcelas deve ser maior que 0.");
                    return;
                }
            }
            Sale.SaleDate = SaleDate;
            Sale.TypePayment = GenerateTypePayment();
            Sale.Installment = IsInstallment;
            Sale.DueDate = DueDate;
            if (IsInstallment)                
                Sale.NumberInstallments = NumberInstallments;
            else                            
                Sale.NumberInstallments = 1;

            foreach (var item in Products)
            {
                item.Sale = Sale;
            }

            await Nav.Navigation.PushAsync(new FinishNewSalePage(Sale, Products));
        }

        private TypePayment GenerateTypePayment()
        {
            switch (TypePayment)
            {
                case "Dinheiro":
                    return Models.Enum.TypePayment.Cash;
                case "Cartão de crédito":
                    return Models.Enum.TypePayment.CreditCard;
                case "Cartão de débito":
                    return Models.Enum.TypePayment.DebitCard;
                case "Á prazo":
                    return Models.Enum.TypePayment.Financing;
                default:
                    return Models.Enum.TypePayment.CreditCard;
            }
        }
    }
}
