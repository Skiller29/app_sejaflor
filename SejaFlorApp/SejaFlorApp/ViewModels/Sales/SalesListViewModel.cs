﻿using SejaFlorApp.Models;
using SejaFlorApp.Services;
using SejaFlorApp.Views.Sales;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows.Input;
using Xamarin.Forms;

namespace SejaFlorApp.ViewModels.Sales
{
    public class SalesListViewModel : BaseViewModel 
    {
        public ICommand NewSaleCommand { get; set; }
        public SaleService _saleService { get; set; }

        private DateTime? _search;
        public DateTime? Search
        {
            get
            {
                return _search;
            }

            set
            {
                _search = value;
                OnPropertyChanged("Search");
                FilterSalesByDate(_search);
            }
        }

        private ObservableCollection<Sale> _sales;
        public ObservableCollection<Sale> Sales
        {
            get
            {
                return _sales;
            }

            set
            {
                _sales = value;
                OnPropertyChanged("Sales");
            }
        }

        private Sale _sale;
        public Sale Sale
        {
            get
            {
                return _sale;
            }

            set
            {
                _sale = value;
                OnPropertyChanged("Sale");
                if (_sale != null)
                {
                    NavigateToDetailsSale(_sale);
                    Sale = null;
                }
                    
            }
        }

        public ObservableCollection<Sale> OriginSales { get; set; }

        public SalesListViewModel()
        {
            _saleService = new SaleService();
            this.NewSaleCommand = new Command(NavigateToNewSale);
            Search = null;
            GetSales();
            MessagingCenter.Subscribe<string>(this, "UpdateListSales", (model) =>
            {
                GetSales();
            });
        }

        private async void GetSales()
        {
            IsBusy = true;
            var sales = await _saleService.ListAllAsync();
            if(sales != null)
            {
                Sales = new ObservableCollection<Sale>(sales.OrderByDescending(x => x.SaleDate).ToList());
                OriginSales = Sales;
            }
            IsBusy = false;    
        }

        private async void NavigateToNewSale()
        {
            await Nav.Navigation.PushAsync(new NewSalePage());
        }
        
        private async void NavigateToDetailsSale(Sale sale)
        {
            await Nav.Navigation.PushAsync(new DetailsSalePage(sale));
        }

        private void FilterSalesByDate(DateTime? date)
        {
            if (date == null)
                Sales = OriginSales;
            else
            {
                var sales = OriginSales.Where(x => x.SaleDate.Date == date.Value.Date).OrderByDescending(x => x.SaleDate).ToList();
                if (sales != null && sales.Any())
                    Sales = new ObservableCollection<Sale>(sales);
                else
                    Sales = new ObservableCollection<Sale>();
            }                       
        }
    }
}
