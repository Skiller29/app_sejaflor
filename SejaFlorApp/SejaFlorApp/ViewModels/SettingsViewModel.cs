﻿using Acr.UserDialogs;
using PCLExt.FileStorage;
using PCLExt.FileStorage.Folders;
using Plugin.FilePicker;
using SejaFlorApp.Interfaces;
using System;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;

namespace SejaFlorApp.ViewModels
{
    public class SettingsViewModel : BaseViewModel
    {
        public ICommand BackupCommand { get; set; }
        public ICommand ImportDataCommand { get; set; }

        private readonly IRepository _repository;

        public SettingsViewModel()
        {
            this.BackupCommand = new Command(Backup);
            this.ImportDataCommand = new Command(ImportData);
            _repository = DependencyService.Get<IRepository>();
        }

        private async void Backup()
        {
            var p = await UserDialogs.Instance.ConfirmAsync($"O arquivo de backup será salvo em Downloads. Deseja continuar?", null, "Sim", "Não");
            if (p)
            {
                IsBusy = true;
                var result = await _repository.MakeBackupAndSaveInDownloadsPathAsync();
                if (result)
                    ShowMessage("Backup feito com sucesso.");
                else
                    ShowMessage("Ocorreu um erro ao fazer o backup.");

                IsBusy = false;
            }
        }

        private async void ImportData()
        {
            var fileTypes = new string[] { ".db3" };
            try
            {
                var p = await UserDialogs.Instance.ConfirmAsync($"Se você importar um backup, perderá os dados que já estão salvos. Deseja continuar?", null, "Sim", "Não");
                if (p)
                {
                    IsBusy = true;
                    var pickedFile = await CrossFilePicker.Current.PickFile(fileTypes);
                    if (pickedFile != null)
                    {

                        pickedFile.FileName = "banco.db3";
                        var result = await _repository.ImportDatabase(pickedFile);
                        if (result)
                        {
                            await Task.Delay(2000);
                            ShowMessage("Backup importado com sucesso.");
                        }
                            
                        else
                        {
                            await Task.Delay(2000);
                            ShowMessage("Ocorreu um erro ao importar o backup.");
                        }
                            

                    }
                    else
                        ShowMessage("O formato do backup é inválido.");

                    IsBusy = false;
                }              
            }
            catch (Exception ex)
            {
                ShowMessage("Ocorreu um erro ao escolher o backup.");
                IsBusy = false;
            }
        }
    }
}
