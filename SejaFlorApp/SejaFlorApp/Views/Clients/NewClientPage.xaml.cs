﻿using SejaFlorApp.ViewModels.Clients;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace SejaFlorApp.Views.Clients
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class NewClientPage : ContentPage
	{
		public NewClientPage ()
		{
			InitializeComponent ();
            this.BindingContext = new NewClientViewModel();
		}
	}
}