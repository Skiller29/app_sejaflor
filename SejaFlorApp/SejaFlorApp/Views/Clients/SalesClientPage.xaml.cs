﻿using SejaFlorApp.ViewModels.Clients;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace SejaFlorApp.Views.Clients
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class SalesClientPage : ContentPage
	{
		public SalesClientPage (int idClient)
		{
			InitializeComponent ();
            this.BindingContext = new SalesClientViewModel(idClient);
		}
	}
}