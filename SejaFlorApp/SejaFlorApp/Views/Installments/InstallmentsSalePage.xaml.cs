﻿using SejaFlorApp.ViewModels;
using SejaFlorApp.ViewModels.Installments;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace SejaFlorApp.Views.Installments
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class InstallmentsSalePage : ContentPage
	{
		public InstallmentsSalePage (int idSale)
		{
			InitializeComponent ();
            this.BindingContext = new InstallmentsSaleViewModel(idSale);
		}
	}
}