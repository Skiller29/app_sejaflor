﻿using Rg.Plugins.Popup.Pages;
using SejaFlorApp.Models;
using SejaFlorApp.ViewModels.Installments;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace SejaFlorApp.Views.Installments
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class PayInstallmentPage : PopupPage
	{
		public PayInstallmentPage (Installment installment)
		{
			InitializeComponent ();
            this.BindingContext = new PayInstallmentViewModel(installment);
		}
	}
}