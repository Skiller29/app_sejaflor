﻿using SejaFlorApp.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace SejaFlorApp.Views
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class MasterPage : ContentPage
	{
		public MasterPage ()
		{
			InitializeComponent ();
            var lista = new List<Item>();
            lista.Add(new Item() { Option = "Home" });
            lista.Add(new Item() { Option = "Vendas" });
            lista.Add(new Item() { Option = "Clientes" });
            lista.Add(new Item() { Option = "Produtos" });
            lista.Add(new Item() { Option = "Relatório" });
            lista.Add(new Item() { Option = "Configurações" });

            listView.ItemsSource = lista;
            this.BindingContext = new MasterViewModel();
        }
	}

    public class Item
    {
        public string Option { get; set; }
    }
}