﻿using SejaFlorApp.ViewModels.Products;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace SejaFlorApp.Views.Products
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class EditProductPage : ContentPage
	{
		public EditProductPage (int idProduct)
		{
			InitializeComponent ();
            this.BindingContext = new EditProductViewModel(idProduct);
		}
	}
}