﻿using SejaFlorApp.ViewModels.Products;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace SejaFlorApp.Views.Products
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class NewProductPage : ContentPage
	{
		public NewProductPage ()
		{
			InitializeComponent ();
            this.BindingContext = new NewProductViewModel();
		}
	}
}