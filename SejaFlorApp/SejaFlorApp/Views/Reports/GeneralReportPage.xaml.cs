﻿using SejaFlorApp.Models;
using SejaFlorApp.ViewModels.Reports;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace SejaFlorApp.Views.Reports
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class GeneralReportPage : TabbedPage
    {
        public GeneralReportPage(List<Installment> installments, string total, string totalReceived, string profitReceived, string totalSold, string profitSold)
        {
            InitializeComponent();
            this.BindingContext = new GeneralReportViewModel(installments, total, totalReceived, profitReceived, totalSold, profitSold);
        }
    }
}