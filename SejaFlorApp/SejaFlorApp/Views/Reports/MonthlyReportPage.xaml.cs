﻿using SejaFlorApp.ViewModels.Reports;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace SejaFlorApp.Views.Reports
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class MonthlyReportPage : ContentPage
	{
		public MonthlyReportPage ()
		{
			InitializeComponent ();
            this.BindingContext = new MonthlyReportViewModel();
		}
	}
}