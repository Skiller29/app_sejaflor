﻿using SejaFlorApp.Models;
using SejaFlorApp.ViewModels.Sales;
using System.Collections.Generic;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace SejaFlorApp.Views.Sales
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class FinishNewSalePage : ContentPage
	{
		public FinishNewSalePage (Sale sale, List<ItemSale> products)
		{
			InitializeComponent ();
            this.BindingContext = new FinishNewSaleViewModel(sale, products);
		}
	}
}