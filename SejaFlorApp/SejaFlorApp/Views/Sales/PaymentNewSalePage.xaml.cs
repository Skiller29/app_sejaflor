﻿using SejaFlorApp.Models;
using SejaFlorApp.ViewModels.Sales;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace SejaFlorApp.Views.Sales
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class PaymentNewSalePage : ContentPage
	{
		public PaymentNewSalePage(Sale sale, List<ItemSale> products)
		{
			InitializeComponent();
            this.BindingContext = new PaymentNewSaleViewModel(sale, products);
		}
	}
}