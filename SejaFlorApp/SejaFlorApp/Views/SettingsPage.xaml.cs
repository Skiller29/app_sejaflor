﻿using SejaFlorApp.ViewModels;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace SejaFlorApp.Views
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class SettingsPage : ContentPage
	{
		public SettingsPage ()
		{
			InitializeComponent ();
            this.BindingContext = new SettingsViewModel();
		}
	}
}